//
//  AppAssembler.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 02/12/2020.
//

import Foundation
import UIKit

/// Entry point of the app
/// If the location access already granted, we are opening the map immediatelly
final class AppAssembler: Assemblable {
    private let locationManager: LocationManager

    init(locationManager: LocationManager) {
        self.locationManager = locationManager
    }

    func assemble() -> ViewRepresentable {
        let navigation = UINavigationController()
        let controller: ViewRepresentable

        if [.authorizedAlways, .authorizedWhenInUse]
            .contains(locationManager.authorizationStatus) {
            controller = MapAssembler(navigation: navigation).assemble()
        } else {
            controller = LocationRequestAssembler(navigation: navigation).assemble()
        }

        navigation.viewControllers = [controller.presentable]

        return navigation
    }
}
