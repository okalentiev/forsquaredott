//
//  AppDelegate.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 23/11/2020.
//

import CoreLocation
import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var appAssembler: Assemblable {
        AppAssembler(locationManager: CLLocationManager())
    }

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = appAssembler.assemble().presentable
        window?.makeKeyAndVisible()

        return true
    }

}
