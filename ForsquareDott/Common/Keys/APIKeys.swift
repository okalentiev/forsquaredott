//
//  APIKeys.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

enum APIKeys {
    static let clientID = "PASTE_CLIENT_HERE"
    static let clientSecret = "PASTE_SECRET_HERE"
}
