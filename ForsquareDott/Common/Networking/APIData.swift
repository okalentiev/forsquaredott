//
//  APIData.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

// MARK: - Response
struct APIData<T>: Decodable where T: Decodable {
    let response: T
}
