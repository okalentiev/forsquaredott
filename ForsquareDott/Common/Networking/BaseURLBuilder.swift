//
//  BaseURLBuilder.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

protocol BaseURLBuildable {
    func build(with path: String) -> URLComponents
}

final class BaseURLBuilder: BaseURLBuildable {
    func build(with path: String) -> URLComponents {
        guard var urlComponents = URLComponents(string: FoursquareAPI.Base.get()) else {
            fatalError("Could not construct base url")
        }

        urlComponents.path.append(path)
        urlComponents.queryItems = [
            URLQueryItem(name: FoursquareAPI.Base.clientIdKey, value: APIKeys.clientID),
            URLQueryItem(name: FoursquareAPI.Base.clientSecret, value: APIKeys.clientSecret)
        ]

        return urlComponents
    }
}
