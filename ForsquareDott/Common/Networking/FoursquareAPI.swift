//
//  FoursquareAPI.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

enum FoursquareAPI {
    enum Base {
        static func get() -> String { "https://api.foursquare.com/v2" }

        static let clientIdKey = "client_id"
        static let clientSecret = "client_secret"
        static let versionKey = "v"
    }

    enum Venues {
        static func get() -> String { "/venues" }

        // swiftlint:disable:next nesting
        enum Explore {
            static func get() -> String { Venues.get() + "/explore" }

            static let sectionKey = "section"
            static let coordinatesKey = "ll"
            static let radiusKey = "radius"
        }
    }
}
