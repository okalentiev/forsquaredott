//
//  NetworkDataProvider.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation
import UIKit

enum NetworkDataProviderError: Error {
    case unknownError
    case responseError(description: String)
    case serializationError(description: String)
}

typealias NetworkDataCompletion<ModelType> = ((ModelType?, Error?) -> Void)

protocol NetworkDataProviderProtocol {
    func get<ModelType: Decodable>(url: URL, with completion: @escaping NetworkDataCompletion<ModelType>)
    func getImage(url: URL, with completion: @escaping NetworkDataCompletion<UIImage>)
}

/**
 Basic network data provider with abilities to fetch images and perform get requests
 that expect Decodable
 */
final class NetworkDataProvider: NetworkDataProviderProtocol {
    private let urlSession: URLSession
    fileprivate let jsonDecoder: JSONDecoder

    init(urlSession: URLSession, jsonDecoder: JSONDecoder) {
        self.urlSession = urlSession
        jsonDecoder.dateDecodingStrategy = .secondsSince1970
        self.jsonDecoder = jsonDecoder
    }

    func get<ModelType: Decodable>(url: URL, with completion: @escaping NetworkDataCompletion<ModelType>) {
        getData(url: url) { [weak self] (data, error) in
            if let responseData = data {
                self?.mapResponse(responseData: responseData, completion: completion)
            } else {
                completion(nil, error)
            }
        }
    }

    func getImage(url: URL, with completion: @escaping NetworkDataCompletion<UIImage>) {
        getData(url: url) { (data, error) in
            if let responseData = data,
                let image = UIImage(data: responseData) {
                completion(image, nil)
            } else {
                completion(nil, error ?? NetworkDataProviderError.unknownError)
            }
        }
    }

    private func getData(url: URL, with completion: @escaping NetworkDataCompletion<Data>) {
        var request = URLRequest(url: url)
        if let code = Locale.current.languageCode {
            // This adds support for internationalisation
            // https://developer.foursquare.com/docs/places-api/internationalization/
            request.addValue(code, forHTTPHeaderField: "Accept-Language")
        }

        let dataTask = urlSession.dataTask(with: request) { (data, _, error) in
            if let responseData = data {
                completion(responseData, nil)
            } else if let responseError = error {
                completion(nil, NetworkDataProviderError.responseError(description:
                    responseError.localizedDescription))
            } else {
                completion(nil, NetworkDataProviderError.unknownError)
            }
        }

        dataTask.resume()
    }
}

// MARK: - Helpers
private extension NetworkDataProvider {
    func mapResponse<ModelType: Decodable>(responseData: Data,
                                           completion: @escaping NetworkDataCompletion<ModelType>) {
        do {
            let decoded = try jsonDecoder.decode(APIData<ModelType>.self, from: responseData)
            completion(decoded.response, nil)
        } catch let error {
            completion(nil, error)
        }
    }
}
