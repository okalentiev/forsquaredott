//
//  Assemblable.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 02/12/2020.
//

import Foundation
import UIKit

/// Protocols that standartises behaviour of the Assemblers
protocol ViewRepresentable {
    var presentable: UIViewController { get }
}

protocol Assemblable {
    func assemble() -> ViewRepresentable
}

extension UIViewController: ViewRepresentable {
    var presentable: UIViewController { self }
}
