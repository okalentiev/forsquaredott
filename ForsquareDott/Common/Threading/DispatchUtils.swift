//
//  DispatchUtils.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

final class DispatchUtils {
    static func renderUI(using closure: @escaping () -> Void) {
        if OperationQueue.current?.underlyingQueue?.label == DispatchQueue.main.label {
            closure()
        } else {
            DispatchQueue.main.async(execute: closure)
        }
    }
}
