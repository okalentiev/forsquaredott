//
//  UIApplication+Protocolisable.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation
import UIKit

/// Wrapper of UIApplication
protocol Application {
    func open(_ url: URL)
    func canOpenURL(_ url: URL) -> Bool
}

extension UIApplication: Application {
    func open(_ url: URL) {
        open(url, options: [:], completionHandler: nil)
    }
}
