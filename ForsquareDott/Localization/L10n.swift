//
//  L10n.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 02/12/2020.
//

import Foundation

/// Idea stolen from SwiftGen
/// Ideally this would be automatically generated based on strings files
enum L10n {

    enum Greeting {
        static var welcome: String {
            "greeting.welcome".localized
        }
    }
    enum Location {
        static var allow: String {
            "location.allow".localized
        }
        static var settings: String {
            "location.settings".localized
        }
        static var access: String {
            "location.access".localized
        }
        static var `description`: String {
            "location.description".localized
        }
        static var deniedDescription: String {
            "location.deniedDescription".localized
        }
    }

    enum Details {
        static var website: String {
            "details.website".localized
        }

        static var call: String {
            "details.call".localized
        }

        static func reviews(_ count: Int) -> String {
            // TODO:oleksii Use stringsdict here
            return String(format: "details.reviews".localized, count)
        }
    }
}
