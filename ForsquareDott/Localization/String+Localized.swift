//
//  String+Localized.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 02/12/2020.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
