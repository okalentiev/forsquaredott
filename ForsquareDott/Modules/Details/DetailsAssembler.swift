//
//  DetailsAssembler.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation
import UIKit

final class DetailsAssembler: Assemblable {
    private let venueId: String

    init(venueId: String) {
        self.venueId = venueId
    }

    func assemble() -> ViewRepresentable {
        let networkDataProvider = NetworkDataProvider(urlSession: .shared,
                                                      jsonDecoder: JSONDecoder())
        let viewModel = DetailsViewModel(placeId: venueId,
                                         detailsRepository: DetailsRepository(networkDataProvider: networkDataProvider,
                                                                              urlBuilder: BaseURLBuilder()),
                                         photoRepository: PhotoRepository(networkDataProvider: networkDataProvider),
                                         phoneService: PhoneService(application: UIApplication.shared),
                                         safariService: SafariService(application: UIApplication.shared))
        let details = DetailsViewController(viewHandler: viewModel)
        viewModel.view = details

        return details
    }
}
