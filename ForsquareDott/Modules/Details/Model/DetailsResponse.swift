//
//  DetailsResponse.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

// MARK: - ResponseClass
struct DetailsResponse: Decodable {
    let venue: VenueDetails
}
