//
//  VenueDetails.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

// MARK: - Venue
// Looks very much like venue from explore call,
// but some of the values are missing from the that response
// and I didn't want to make them optional so created a new one
// Best would be to reuse common with a protocol IMO
struct VenueDetails: Decodable {
    let venueId, name: String
    let contact: Contact
    let url: String?
    let rating: Double
    let tips: Tips
    let bestPhoto: BestPhoto

    private enum CodingKeys: String, CodingKey {
        case venueId = "id"
        case name, contact, url, rating, tips, bestPhoto
    }
}

// MARK: - BestPhoto
struct BestPhoto: Decodable {
    let bestPhotoPrefix: String
    let suffix: String

    enum CodingKeys: String, CodingKey {
        case bestPhotoPrefix = "prefix"
        case suffix
    }
}

// MARK: - Contact
struct Contact: Decodable {
    let phone: String?
    let formattedPhone: String?
}

// MARK: - Tips
struct Tips: Decodable {
    let count: Int
}
