//
//  DetailsRepository.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

typealias DetailsCompletion = ((VenueDetails?, Error?) -> Void)

/// Fetches place details from FoursquareAPI
final class DetailsRepository {
    private static let detailsAPIVersion = "20201203"

    private let networkDataProvider: NetworkDataProvider
    private let urlBuilder: BaseURLBuildable

    init(networkDataProvider: NetworkDataProvider,
         urlBuilder: BaseURLBuildable) {
        self.networkDataProvider = networkDataProvider
        self.urlBuilder = urlBuilder
    }

    func details(placeId: String, with completion: @escaping DetailsCompletion) {
        networkDataProvider.get(url: detailsUrl(venueId: placeId)) { (detailsResult: DetailsResponse?, error) in
            if let venue = detailsResult?.venue {
                completion(venue, nil)
            } else {
                completion(nil, error)
            }
        }
    }
}

private extension DetailsRepository {
    func detailsUrl(venueId: String) -> URL {
        var urlComponents = urlBuilder.build(with: FoursquareAPI.Venues.get() + "/\(venueId)")

        let version = URLQueryItem(name: FoursquareAPI.Base.versionKey,
                                   value: DetailsRepository.detailsAPIVersion)
        urlComponents.queryItems?.append(version)

        guard let detailsUrl = urlComponents.url else {
            fatalError("Could not construct details url")
        }

        return detailsUrl
    }
}
