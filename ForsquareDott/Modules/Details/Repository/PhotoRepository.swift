//
//  PhotoRepository.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation
import UIKit

typealias PhotoCompletion = ((UIImage?, Error?) -> Void)

/// Downloads  place image from Places API for the provided width.
/// Does not cache the image.
final class PhotoRepository {
    private let networkDataProvider: NetworkDataProvider

    init(networkDataProvider: NetworkDataProvider) {
        self.networkDataProvider = networkDataProvider
    }

    func photo(prefix: String, suffix: String, with completion: @escaping PhotoCompletion) {
        networkDataProvider
            .getImage(url: photoUrl(prefix: prefix, suffix: suffix), with: completion)
    }
}

private extension PhotoRepository {
    func photoUrl(prefix: String, suffix: String) -> URL {
        // Hardcoding size for now, ideally would calculate it based on the phone resolution
        let urlString = prefix + "300x500" + suffix

        guard let photoUrl = URL(string: urlString) else {
            fatalError("Could not construct photo url")
        }

        return photoUrl
    }
}
