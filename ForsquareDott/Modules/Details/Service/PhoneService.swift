//
//  PhoneService.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

/// A service that will prompt user to call to the provided phone number.
final class PhoneService {
    private let application: Application

    init(application: Application) {
        self.application = application
    }

    func call(phone: String) {
        // TODO:oleksii Add simulator handling
        guard let phoneURL = URL(string: "telprompt://\(phone)"),
            application.canOpenURL(phoneURL) else {
            return
        }

        application.open(phoneURL)
    }
}
