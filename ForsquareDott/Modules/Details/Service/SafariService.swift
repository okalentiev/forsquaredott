//
//  SafariService.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

/**
 Out of simplicity made this open a url leaving the app.
 The best would be to use SFSafariViewController to avoid context switching
 */
final class SafariService {
    private let application: Application

    init(application: Application) {
        self.application = application
    }

    func open(website: String) {
        guard let url = URL(string: website),
            application.canOpenURL(url) else {
            return
        }

        application.open(url)
    }
}
