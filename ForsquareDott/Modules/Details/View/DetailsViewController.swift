//
//  DetailsViewController.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import UIKit

protocol DetailsViewProtocol: class {
    func updateTitle(title: String)
    func showPhoto(image: UIImage)
    func showPhone(phone: String)
    func showWebsite(webSite: String)
    func showRating(rating: String)
    func showReviewCount(count: String)
    func displayPhoneButton(selector: Selector)
    func displayWebsiteButton(selector: Selector)

    func startLoading()
    func stopLoading()
}

/// Venue details screen
/// - Backdrop image
/// - Loading indicator
/// - Phone number and website
/// - Rating
/// - CTA
/// - Passes actions to viewHandler
class DetailsViewController: UIViewController {
    private var viewHandler: DetailsViewHandlerProtocol?

    convenience init(viewHandler: DetailsViewHandlerProtocol) {
        self.init(nibName: nil, bundle: nil)
        self.viewHandler = viewHandler
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHandler?.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func loadView() {
        view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = .systemBackground

        // ScrollView
        view.addSubview(scrollView)
        [
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)].activate()

        // Stack view
        scrollView.addSubview(detailsStackView)
        detailsStackView.spacing = DetailsViewController.verticalStackSpacing
        [
            detailsStackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            detailsStackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            detailsStackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            detailsStackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            detailsStackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor,
                                              multiplier: DetailsViewController.backDropPhotoRatio)
        ].activate()

        // Activity indicator
        view.addSubview(activityIndicator)
        [
            activityIndicator.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor)
        ].activate()
    }

    private static let backDropPhotoRatio: CGFloat = 0.6
    private static let verticalStackSpacing: CGFloat = 10
    private static let ctaButtonHeight: CGFloat = 44
    private static let infoIconSize: CGFloat = 30.0

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.hidesWhenStopped = true
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()

    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private var detailsStackView: UIStackView = {
        let detailsStackView = UIStackView()
        detailsStackView.axis = .vertical
        detailsStackView.alignment = .leading
        detailsStackView.translatesAutoresizingMaskIntoConstraints = false
        return detailsStackView
    }()
}

// MARK: - ViewProtocol
extension DetailsViewController: DetailsViewProtocol {
    func updateTitle(title: String) {
        self.title = title
    }

    func showPhoto(image: UIImage) {
        // TODO:oleksii Show loading indicator for image
        detailsStackView.insertArrangedSubview(imageView, at: 0)

        imageView.alpha = 0
        imageView.image = image
        UIView.animate(withDuration: 0.3) {
            self.imageView.alpha = 1
        }
    }

    func showPhone(phone: String) {
        let phoneButton = messageLabel(text: phone)
        detailsStackView.addArrangedSubview(rowStackView(with: "phone.circle",
                                                         view: phoneButton))
    }

    func showWebsite(webSite: String) {
        let websiteButton = messageLabel(text: webSite)
        detailsStackView.addArrangedSubview(rowStackView(with: "safari",
                                                         view: websiteButton))
    }

    func showRating(rating: String) {
        let ratingLabel = messageLabel(text: rating)
        detailsStackView.addArrangedSubview(rowStackView(with: "star.lefthalf.fill",
                                                         view: ratingLabel))
    }

    func showReviewCount(count: String) {
        let reviewCount = messageLabel(text: count)
        detailsStackView.addArrangedSubview(rowStackView(with: "pencil",
                                                         view: reviewCount))
    }

    func displayPhoneButton(selector: Selector) {
        configureCTAButton(imageName: "phone.arrow.up.right",
                           title: L10n.Details.call,
                           color: .systemGreen,
                           selector: selector)
    }

    func displayWebsiteButton(selector: Selector) {
        configureCTAButton(imageName: "safari",
                           title: L10n.Details.website,
                           color: .systemBlue,
                           selector: selector)
    }

    func startLoading() {
        activityIndicator.startAnimating()
    }

    func stopLoading() {
        activityIndicator.stopAnimating()
    }
}

// MARK: View Helpers
private extension DetailsViewController {
    func rowStackView(with imageName: String, view: UIView) -> UIView {
        let iconImage = UIImageView(image: UIImage(systemName: imageName))
        iconImage.contentMode = .scaleAspectFit
        NSLayoutConstraint.activate([
            iconImage.widthAnchor.constraint(equalToConstant: DetailsViewController.infoIconSize),
            iconImage.heightAnchor.constraint(equalToConstant: DetailsViewController.infoIconSize)
        ])

        let stackView = UIStackView(arrangedSubviews: [iconImage, view])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = view.layoutMargins.left

        return wrapStackView(stackView: stackView)
    }

    func wrapStackView(stackView: UIStackView) -> UIView {
        let containerView = UIView()
        containerView.addSubview(stackView)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.topAnchor, constant: view.layoutMargins.left),
            stackView.bottomAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.bottomAnchor, constant: view.layoutMargins.left),
            stackView.leadingAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.leadingAnchor, constant: view.layoutMargins.left),
            stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: view.layoutMargins.left)
        ])
        return containerView
    }

    func messageLabel(text: String) -> UILabel {
        let messageLabel = UILabel()
        messageLabel.font = .preferredFont(forTextStyle: .title3)
        messageLabel.textColor = .label
        messageLabel.text = text

        return messageLabel
    }

    func configureCTAButton(imageName: String, title: String, color: UIColor, selector: Selector) {
        let ctaButton = UIButton()
        ctaButton.translatesAutoresizingMaskIntoConstraints = false
        ctaButton.setImage(UIImage(systemName: imageName), for: .normal)
        ctaButton.setTitle(title, for: .normal)
        ctaButton.backgroundColor = color
        ctaButton.tintColor = .systemBackground
        ctaButton.layer.cornerRadius = DetailsViewController.ctaButtonHeight / 4
        ctaButton.addTarget(viewHandler, action: selector, for: .touchUpInside)

        let containerView = UIView()
        containerView.addSubview(ctaButton)
        detailsStackView.addArrangedSubview(containerView)

        NSLayoutConstraint.activate([
            ctaButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: view.layoutMargins.left),
            ctaButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: view.layoutMargins.left),
            ctaButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: view.layoutMargins.left),
            ctaButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: view.layoutMargins.left),
            ctaButton.heightAnchor.constraint(equalToConstant: DetailsViewController.ctaButtonHeight),
            ctaButton.widthAnchor.constraint(equalTo: detailsStackView.widthAnchor, constant: -(2 * view.layoutMargins.left))
            ])
    }
}
