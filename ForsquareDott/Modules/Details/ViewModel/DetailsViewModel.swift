//
//  DetailsViewModel.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

protocol DetailsViewHandlerProtocol {
    func reloadData()
}

/// Contains business logic related to displaying the details of the Place.
/// - Dynamicly displays info based on what is available.
/// - Loads image of the place
/// - Dials the phone number using `PhoneService`
/// - Opens webstite using `SafariService`
final class DetailsViewModel: DetailsViewHandlerProtocol {
    private let placeId: String

    private var details: VenueDetails?

    private let detailsRepository: DetailsRepository
    private let photoRepository: PhotoRepository
    private let phoneService: PhoneService
    private let safariService: SafariService

    weak var view: DetailsViewProtocol?

    init(placeId: String,
         detailsRepository: DetailsRepository,
         photoRepository: PhotoRepository,
         phoneService: PhoneService,
         safariService: SafariService) {
        self.placeId = placeId
        self.detailsRepository = detailsRepository
        self.photoRepository = photoRepository
        self.phoneService = phoneService
        self.safariService = safariService
    }

    func reloadData() {
        view?.startLoading()
        detailsRepository.details(placeId: placeId) { [weak self] (placeDetails, _) in
            if let details = placeDetails {
                self?.loadPhoto(bestPhoto: details.bestPhoto)
                DispatchUtils.renderUI {
                    self?.details = details
                    self?.fillContent(details: details)
                    self?.handleCTA()
                }
            }
            DispatchUtils.renderUI {
                self?.view?.stopLoading()
            }
        }
    }

    @objc
    func openWebSite() {
        guard let website = details?.url else {
            return
        }
        safariService.open(website: website)
    }

    @objc
    func call() {
        guard let phone = details?.contact.phone else {
            return
        }
        phoneService.call(phone: phone)
    }
}

private extension DetailsViewModel {
    func loadPhoto(bestPhoto: BestPhoto) {
        photoRepository.photo(prefix: bestPhoto.bestPhotoPrefix,
                              suffix: bestPhoto.suffix) { [weak self] (image, _) in
            if let photo = image {
                DispatchUtils.renderUI {
                    self?.view?.showPhoto(image: photo)
                }
            }
        }
    }

    func fillContent(details: VenueDetails) {
        if let phone = details.contact.formattedPhone {
            view?.showPhone(phone: phone)
        }

        if let urlString = details.url,
           let url = URL(string: urlString),
           let host = url.host {
            view?.showWebsite(webSite: host)
        }

        // Place rating
        view?.showRating(rating: String(format: "%.1f", details.rating))

        // Review count
        view?.showReviewCount(count: "Ratings: \(details.tips.count)")

        view?.updateTitle(title: details.name)
    }

    func handleCTA() {
        if details?.contact.phone != nil {
            view?.displayPhoneButton(selector: #selector(call))
        }
        if details?.url != nil {
            view?.displayWebsiteButton(selector: #selector(openWebSite))
        }
    }
}
