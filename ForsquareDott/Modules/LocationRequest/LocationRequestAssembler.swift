//
//  OnboardingCoordinator.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 26/11/2020.
//

import CoreLocation
import Foundation
import UIKit

final class LocationRequestAssembler: Assemblable {
    private weak var navigation: UINavigationController?

    init(navigation: UINavigationController) {
        self.navigation = navigation
    }

    func assemble() -> ViewRepresentable {
        let viewHandler = LocationRequestViewModel(locationManager: CLLocationManager(),
                                                   application: UIApplication.shared) {
            guard let navigation = self.navigation else {
                assertionFailure("Missing navigation controller")
                return
            }
            navigation.pushViewController(MapAssembler(navigation: navigation).assemble().presentable,
                                          animated: true)
        }
        let locationRequest = LocationRequestViewController(viewHandler: viewHandler)
        viewHandler.view = locationRequest

        return locationRequest
    }
}
