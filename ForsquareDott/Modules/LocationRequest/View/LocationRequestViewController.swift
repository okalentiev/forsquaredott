//
//  OnboardingViewController.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 26/11/2020.
//

import UIKit

protocol LocationRequestView: class {
    func descriptionUpdated(to text: String)
    func ctaChanged(to text: String)
}

/// Provides user friendly explanation for the Location request
final class LocationRequestViewController: UIViewController {
    private var viewHandler: LocationRequestViewHandler?

    convenience init(viewHandler: LocationRequestViewHandler) {
        self.init(nibName: nil, bundle: nil)
        self.viewHandler = viewHandler
    }

    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textColor = .label
        descriptionLabel.font = .preferredFont(forTextStyle: .title3)
        return descriptionLabel
    }()

    private lazy var ctaButton: UIButton = {
        let allowButton = UIButton(primaryAction: UIAction { [weak self] _ in
            self?.viewHandler?.ctaTapped()
        })
        allowButton.translatesAutoresizingMaskIntoConstraints = false
        allowButton.layer.borderWidth = 1.0
        allowButton.layer.borderColor = UIColor.systemBlue.cgColor
        return allowButton
    }()

    override func loadView() {
        view = UIView()
        view.backgroundColor = .systemBackground

        let locationRequestLabel = UILabel()
        locationRequestLabel.text = L10n.Location.access
        locationRequestLabel.numberOfLines = 0
        locationRequestLabel.textColor = .label
        locationRequestLabel.font = .preferredFont(forTextStyle: .title2)

        let labelsStack = UIStackView(
            arrangedSubviews: [
                locationRequestLabel,
                descriptionLabel
            ]
        )
        labelsStack.translatesAutoresizingMaskIntoConstraints = false
        labelsStack.axis = .vertical

        view.addSubview(labelsStack)
        view.addSubview(ctaButton)
        [
            labelsStack.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            labelsStack.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            labelsStack.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            labelsStack.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.2),
            ctaButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ctaButton.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            ctaButton.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            ctaButton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.07)
        ].activate()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true

        title = L10n.Greeting.welcome
    }
}

extension LocationRequestViewController: LocationRequestView {
    func ctaChanged(to text: String) {
        ctaButton.setTitle(text, for: .normal)
    }

    func descriptionUpdated(to text: String) {
        descriptionLabel.text = text
    }
}
