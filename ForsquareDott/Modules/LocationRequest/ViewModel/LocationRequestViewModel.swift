//
//  LocationRequestViewModel.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 02/12/2020.
//

import CoreLocation
import Foundation
import UIKit

protocol LocationRequestViewHandler {
    func ctaTapped()
}

final class LocationRequestViewModel: NSObject, LocationRequestViewHandler {
    private let locationManager: LocationManager
    private let application: Application
    // Chose block callback here for simplicity
    // Usually would make this a delegate
    private let onSuccess: () -> Void
    weak var view: LocationRequestView?

    init(locationManager: LocationManager,
         application: Application,
         onSuccess: @escaping () -> Void) {
        self.locationManager = locationManager
        self.application = application
        self.onSuccess = onSuccess
        super.init()
        locationManager.delegate = self
    }

    func ctaTapped() {
        guard ![.denied, .restricted]
                .contains(locationManager.authorizationStatus) else {
            URL(string: UIApplication.openSettingsURLString).flatMap(application.open)
            return
        }
        locationManager.requestWhenInUseAuthorization()
    }
}

extension LocationRequestViewModel: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .denied, .restricted:
            view?.descriptionUpdated(to: L10n.Location.deniedDescription)
            view?.ctaChanged(to: L10n.Location.settings)
        case .authorizedAlways, .authorizedWhenInUse:
            onSuccess()
        default:
            setInitial()
        }
    }
}

private extension LocationRequestViewModel {
    func setInitial() {
        view?.descriptionUpdated(to: L10n.Location.description)
        view?.ctaChanged(to: L10n.Location.allow)
    }
}
