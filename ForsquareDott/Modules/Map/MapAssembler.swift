//
//  MapAssembler.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 23/11/2020.
//

import CoreLocation
import Foundation
import UIKit

final class MapAssembler: Assemblable {
    private weak var navigation: UINavigationController?

    init(navigation: UINavigationController) {
        self.navigation = navigation
    }

    func assemble() -> ViewRepresentable {
        let repository = ExploreVenuesNetworkRepository(
            networkDataProvider: NetworkDataProvider(urlSession: URLSession.shared,
                                                     jsonDecoder: JSONDecoder()),
            urlBuilder: BaseURLBuilder()
        )
        let viewModel = MapViewModel(locationManager: CLLocationManager(),
                                     exploreRepository: repository) { venueId in
            self.navigation?.pushViewController(DetailsAssembler(venueId: venueId).assemble().presentable,
                                                 animated: true)
        }
        let mapViewController = MapViewController(viewHandler: viewModel)
        viewModel.view = mapViewController

        return mapViewController
    }
}
