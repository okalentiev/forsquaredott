//
//  Groups.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

// MARK: - ExploreResponse
struct ExploreResponse: Decodable {
    let groups: [Group]
}

// MARK: - Group
struct Group: Decodable {
    let items: [Item]
}

// MARK: - Item
struct Item: Decodable {
    let venue: Venue
}
