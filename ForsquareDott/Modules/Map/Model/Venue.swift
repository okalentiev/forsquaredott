//
//  Venue.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

// MARK: - Venue
struct Venue: Decodable {
    let venueId, name: String
    let location: Location
    let categories: [Category]

    private enum CodingKeys: String, CodingKey {
        case venueId = "id"
        case name, location, categories
    }
}

// MARK: - Category
struct Category: Decodable {
    let shortName: String
}

// MARK: - Location
struct Location: Decodable {
    let address: String
    let lat, lng: Double
}
