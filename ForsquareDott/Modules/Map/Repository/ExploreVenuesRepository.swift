//
//  ExploreVenuesRepository.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

protocol ExploreVenuesRepository {
    func loadSuggestions(for latitude: Double,
                         longitude: Double,
                         radius: Double?,
                         with completion: @escaping ([Venue]?, Error?) -> Void)
}

final class ExploreVenuesNetworkRepository: ExploreVenuesRepository {
    // Not so sure how I feel about hardcoding API version for every view model,
    // but at least this would ensure that nothing breaks in the meantime
    // Though this potentially can cause a lot of issues and confusion in the long term
    private static let mapViewAPIVersion = "20201203"
    private static let defaultSection = "food"

    private let networkDataProvider: NetworkDataProviderProtocol
    private let urlBuilder: BaseURLBuildable

    init(networkDataProvider: NetworkDataProviderProtocol,
         urlBuilder: BaseURLBuildable) {
        self.networkDataProvider = networkDataProvider
        self.urlBuilder = urlBuilder
    }

    func loadSuggestions(for latitude: Double,
                         longitude: Double,
                         radius: Double?,
                         with completion: @escaping ([Venue]?, Error?) -> Void) {
        networkDataProvider.get(url: exploreUrl(with: latitude,
                                                longitude: longitude,
                                                radius: radius)) { (response: ExploreResponse?, error) in
            guard error == nil,
                  let venues = response?
                    .groups
                    .flatMap({ $0.items })
                    .compactMap({ $0.venue }) else {
                completion(nil, error ?? NetworkDataProviderError.unknownError)
                return
            }

            completion(venues, nil)
        }
    }

    private func exploreUrl(with latitude: Double,
                            longitude: Double,
                            radius: Double?) -> URL {
        var urlComponents = urlBuilder.build(with: FoursquareAPI.Venues.Explore.get())

        let version = URLQueryItem(name: FoursquareAPI.Base.versionKey,
                                   value: ExploreVenuesNetworkRepository.mapViewAPIVersion)
        let section = URLQueryItem(name: FoursquareAPI.Venues.Explore.sectionKey,
                                       value: ExploreVenuesNetworkRepository.defaultSection)
        let coordinates = URLQueryItem(name: FoursquareAPI.Venues.Explore.coordinatesKey,
                                       value: [String(latitude), String(longitude)].joined(separator: ","))

        urlComponents
            .queryItems?
            .append(contentsOf: [
                version,
                section,
                coordinates
            ])

        if let radius = radius {
            urlComponents
                .queryItems?
                .append(URLQueryItem(name: FoursquareAPI.Venues.Explore.radiusKey,
                                     value: String(radius)))
        }

        guard let exploreUrl = urlComponents.url else {
            fatalError("Could not construct explore url")
        }

        return exploreUrl
    }
}
