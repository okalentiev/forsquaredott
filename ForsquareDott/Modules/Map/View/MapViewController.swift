//
//  MapViewController.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 26/11/2020.
//

import MapKit
import UIKit

protocol MapView: class {
    var zoomInMeters: CLLocationDistance { get }
    func setCurrentLocation(_ location: CLLocationCoordinate2D)
    func clearAnnotations()
    func annotationsLoaded(_ annotations: [VenueAnnotation])
}

/// Map view that displays restaurants around current user location
/// When user changes map region, view model loads restaurants in the new region
final class MapViewController: UIViewController {
    private var viewHandler: MapViewHandler?

    var zoomInMeters: CLLocationDistance = 200.0

    convenience init(viewHandler: MapViewHandler) {
        self.init(nibName: nil, bundle: nil)
        self.viewHandler = viewHandler
    }

    private var mapView: MKMapView? {
        view as? MKMapView
    }

    override func loadView() {
        view = MKMapView()

        mapView?.delegate = self
        mapView?.showsCompass = true
        mapView?.showsUserLocation = true
        mapView?.pointOfInterestFilter = .excludingAll
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        mapView?.deselectAnnotation(mapView?.selectedAnnotations.first, animated: true)
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? VenueAnnotation else {
            return
        }
        viewHandler?.annotationSelected(annotation)
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        viewHandler?.regionChanged(region: mapView.region)
    }
}

extension MapViewController: MapView {
    func setCurrentLocation(_ location: CLLocationCoordinate2D) {
        let region = MKCoordinateRegion(center: location,
                                        latitudinalMeters: zoomInMeters,
                                        longitudinalMeters: zoomInMeters)
        guard let adjustedRegion = mapView?.regionThatFits(region) else {
            mapView?.setCenter(location, animated: true)
            return
        }
        mapView?.setRegion(adjustedRegion, animated: true)
    }

    func clearAnnotations() {
        guard let annotations = mapView?.annotations else {
            return
        }
        mapView?.removeAnnotations(annotations)
    }

    func annotationsLoaded(_ annotations: [VenueAnnotation]) {
        mapView?.addAnnotations(annotations)
    }
}
