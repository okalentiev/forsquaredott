//
//  VenueAnnotation.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation
import MapKit

final class VenueAnnotation: NSObject, MKAnnotation {
    let venueId: String

    var coordinate: CLLocationCoordinate2D

    var title: String?
    var subtitle: String?

    init(venueId: String,
         latitude: Double,
         longitude: Double,
         title: String?,
         subtitle: String?) {
        self.venueId = venueId
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.title = title
        self.subtitle = subtitle
    }
}
