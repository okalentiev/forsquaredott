//
//  MapViewModel.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation
import MapKit

protocol MapViewHandler {
    func annotationSelected(_ annotation: VenueAnnotation)
    func regionChanged(region: MKCoordinateRegion)
}

private extension MKCoordinateRegion {
    // Approximating current radius for a region
    func distanceMax() -> CLLocationDistance {
        let furthest = CLLocation(latitude: center.latitude + (span.latitudeDelta / 2),
                             longitude: center.longitude + (span.longitudeDelta / 2))
        let centerLoc = CLLocation(latitude: center.latitude, longitude: center.longitude)
        return centerLoc.distance(from: furthest)
    }
}

final class MapViewModel: NSObject, MapViewHandler {
    private let locationManager: LocationManager
    private let exploreRepository: ExploreVenuesRepository

    private var exploreDelayTimer: DispatchWorkItem?

    private let onSuccess: (String) -> Void

    weak var view: MapView?

    init(locationManager: LocationManager,
         exploreRepository: ExploreVenuesRepository,
         onSuccess: @escaping (String) -> Void) {
        self.locationManager = locationManager
        self.exploreRepository = exploreRepository
        self.onSuccess = onSuccess

        super.init()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLHeadingFilterNone
        locationManager.startUpdatingLocation()
    }

    func annotationSelected(_ annotation: VenueAnnotation) {
        onSuccess(annotation.venueId)
    }

    func regionChanged(region: MKCoordinateRegion) {
        exploreDelayTimer?.cancel()

        // Delaying the beginning of explore to avoid unnecessary calls
        let exploreDelayTimer = DispatchWorkItem { [weak self] in
            self?.exploreArea(around: region.center,
                              radius: region.distanceMax())
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5,
                                      execute: exploreDelayTimer)

        self.exploreDelayTimer = exploreDelayTimer
    }
}

extension MapViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = locations.last?.coordinate {
            view?.setCurrentLocation(coordinate)
            exploreArea(around: coordinate,
                        radius: view?.zoomInMeters)
        }
        locationManager.stopUpdatingLocation()
    }
}

private extension MapViewModel {
    func exploreArea(around coordinate: CLLocationCoordinate2D,
                     radius: CLLocationDistance?) {
        exploreRepository.loadSuggestions(for: coordinate.latitude,
                                          longitude: coordinate.longitude,
                                          radius: radius) { [weak self] (venues, _) in
            guard let venues = venues else {
                // TODO:oleksii Handle error
                return
            }

            let annotations = venues.map { VenueAnnotation(venueId: $0.venueId,
                                                           latitude: $0.location.lat,
                                                           longitude: $0.location.lng,
                                                           title: $0.name,
                                                           subtitle: $0.categories.first?.shortName)

            }

            DispatchUtils.renderUI {
                self?.view?.clearAnnotations()
                self?.view?.annotationsLoaded(annotations)
            }
        }
    }
}
