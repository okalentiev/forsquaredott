//
//  main.swift
//  ForsquareDott
//
//  Created by Oleksii Kalentiev on 23/11/2020.
//

import Foundation
import UIKit

let isRunningUnitTests = ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil
let isRunningUITests = ProcessInfo.processInfo.arguments.contains("-ui-testing")
let args = CommandLine.unsafeArgv

if isRunningUnitTests {
    _ = UIApplicationMain(CommandLine.argc, args, nil, NSStringFromClass(AppDelegateUnitTests.self))
} else if isRunningUITests {
    _ = UIApplicationMain(CommandLine.argc, args, nil, NSStringFromClass(AppDelegateUITests.self))
} else {
    _ = UIApplicationMain(CommandLine.argc, args, nil, NSStringFromClass(AppDelegate.self))
}
