//
//  AppDelegateUnitTests.swift
//  ForsquareDottTests
//
//  Created by Oleksii Kalentiev on 23/11/2020.
//

import Foundation
import UIKit

class AppDelegateUnitTests: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
