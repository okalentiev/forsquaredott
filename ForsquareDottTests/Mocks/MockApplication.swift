//
//  MockApplication.swift
//  ForsquareDottTests
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

@testable import ForsquareDott

final class MockApplication: Application {
    func canOpenURL(_ url: URL) -> Bool {
        return false
    }

    var url: URL?
    func open(_ url: URL) {
        self.url = url
    }
}
