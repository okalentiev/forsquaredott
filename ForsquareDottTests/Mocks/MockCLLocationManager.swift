//
//  MockCLLocationManager.swift
//  ForsquareDottTests
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import CoreLocation
import Foundation

final class MockCLLocationManager: CLLocationManager {
    private var overridenAuthStatus: CLAuthorizationStatus?
    convenience init(status: CLAuthorizationStatus) {
        self.init()
        self.overridenAuthStatus = status
    }

    override var authorizationStatus: CLAuthorizationStatus {
        return overridenAuthStatus ?? super.authorizationStatus
    }
}
