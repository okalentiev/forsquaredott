//
//  MockLocationManager.swift
//  ForsquareDottTests
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import CoreLocation
import Foundation

@testable import ForsquareDott

final class MockLocationManager: LocationManager {
    var expectedAuthorizationStatus: CLAuthorizationStatus?
    var authorisationRequested = false

    weak var delegate: CLLocationManagerDelegate?

    var authorizationStatus: CLAuthorizationStatus {
        return expectedAuthorizationStatus ?? .notDetermined
    }

    func requestWhenInUseAuthorization() {
        authorisationRequested = true
    }

    var desiredAccuracy: CLLocationAccuracy = kCLLocationAccuracyBest
    var distanceFilter: CLLocationDistance = kCLDistanceFilterNone

    func startUpdatingLocation() {
    }

    func stopUpdatingLocation() {
    }
}
