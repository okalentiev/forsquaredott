//
//  MockLocationRequestView.swift
//  ForsquareDottTests
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import Foundation

@testable import ForsquareDott

final class MockLocationRequestView: LocationRequestView {
    var receivedDescription: String?
    var receivedCta: String?

    func descriptionUpdated(to text: String) {
        receivedDescription = text
    }

    func ctaChanged(to text: String) {
        receivedCta = text
    }
}
