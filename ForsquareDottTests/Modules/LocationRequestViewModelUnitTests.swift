//
//  LocationRequestViewModelUnitTests.swift
//  ForsquareDottTests
//
//  Created by Oleksii Kalentiev on 03/12/2020.
//

import XCTest

@testable import ForsquareDott

class LocationRequestViewModelUnitTests: XCTestCase {
    //swiftlint:disable implicitly_unwrapped_optional
    var viewModel: LocationRequestViewModel!
    var mockApplication: MockApplication!
    var mockLocationManager: MockLocationManager!
    var mockView: MockLocationRequestView!
    //swiftlint:enable implicitly_unwrapped_optional

    var onSuccessCalled = false

    override func setUp() {
        super.setUp()
        mockApplication = MockApplication()
        mockLocationManager = MockLocationManager()
        mockView = MockLocationRequestView()

        viewModel = LocationRequestViewModel(locationManager: mockLocationManager,
                                             application: mockApplication,
                                             onSuccess: { [weak self] in
                                                self?.onSuccessCalled = true
                                             })
        viewModel.view = mockView
    }

    func testInit_shouldSetDelegateToViewModel() {
        // Given

        // When

        // Then
        XCTAssertNotNil(mockLocationManager.delegate)
    }

    func testCTATapped_withNotDetermined_shouldRequestAuthorisation() {
        // Given

        // When
        viewModel.ctaTapped()

        // Then
        XCTAssertTrue(mockLocationManager.authorisationRequested)
    }

    func testCTATapped_withDenied_shouldOpenSettings() {
        // Given
        mockLocationManager.expectedAuthorizationStatus = .denied

        // When
        viewModel.ctaTapped()

        // Then
        XCTAssertFalse(mockLocationManager.authorisationRequested)
        XCTAssertEqual(mockApplication.url, URL(string: UIApplication.openSettingsURLString))
    }

    func testCTATapped_withRestricted_shouldOpenSettings() {
        // Given
        mockLocationManager.expectedAuthorizationStatus = .restricted

        // When
        viewModel.ctaTapped()

        // Then
        XCTAssertFalse(mockLocationManager.authorisationRequested)
        XCTAssertEqual(mockApplication.url, URL(string: UIApplication.openSettingsURLString))
    }

    func testLocationManagerDidChangeAuthorization_withDenied_shouldUpdateValues() {
        // Given
        let mockCLLocation = MockCLLocationManager(status: .denied)

        // When
        viewModel.locationManagerDidChangeAuthorization(mockCLLocation)

        // Then
        XCTAssertEqual(mockView.receivedDescription, L10n.Location.deniedDescription)
        XCTAssertEqual(mockView.receivedCta, L10n.Location.settings)
        XCTAssertFalse(onSuccessCalled)
    }

    func testLocationManagerDidChangeAuthorization_withRestricted_shouldUpdateValues() {
        // Given
        let mockCLLocation = MockCLLocationManager(status: .restricted)

        // When
        viewModel.locationManagerDidChangeAuthorization(mockCLLocation)

        // Then
        XCTAssertEqual(mockView.receivedDescription, L10n.Location.deniedDescription)
        XCTAssertEqual(mockView.receivedCta, L10n.Location.settings)
        XCTAssertFalse(onSuccessCalled)
    }

    func testLocationManagerDidChangeAuthorization_withAuthorizedAlways_shouldCallOnSuccess() {
        // Given
        let mockCLLocation = MockCLLocationManager(status: .authorizedAlways)

        // When
        viewModel.locationManagerDidChangeAuthorization(mockCLLocation)

        // Then
        XCTAssertTrue(onSuccessCalled)
    }

    func testLocationManagerDidChangeAuthorization_withAuthorizedWhenInUse_shouldCallOnSuccess() {
        // Given
        let mockCLLocation = MockCLLocationManager(status: .authorizedWhenInUse)

        // When
        viewModel.locationManagerDidChangeAuthorization(mockCLLocation)

        // Then
        XCTAssertTrue(onSuccessCalled)
    }

    func testLocationManagerDidChangeAuthorization_withNonDetermined_shouldSetInitial() {
        // Given
        let mockCLLocation = MockCLLocationManager(status: .notDetermined)

        // When
        viewModel.locationManagerDidChangeAuthorization(mockCLLocation)

        // Then
        XCTAssertEqual(mockView.receivedDescription, L10n.Location.description)
        XCTAssertEqual(mockView.receivedCta, L10n.Location.allow)
    }
}
