//
//  AppDelegateUITests.swift
//  ForsquareDottUITests
//
//  Created by Oleksii Kalentiev on 23/11/2020.
//

import Foundation
import UIKit

class AppDelegateUITests: AppDelegate {
    /// The idea behind this was to swap assemblers and inject mocks into the UI tests
    /// Abandoned because of lack of time
    override var appAssembler: Assemblable {
        UITestAssembler()
    }

    override func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
}

final class UITestAssembler: Assemblable {
    func assemble() -> ViewRepresentable {
        let navigation = UINavigationController()
        let locationRequest = LocationRequestAssembler(navigation: navigation).assemble()

        navigation.viewControllers = [locationRequest.presentable]

        return navigation
    }
}
