# ForsquareDott
ForsquareDott is a simple iOS application that displays restaurants around user location the places and call them or check the website. The app is using FoursquareAPI.

# Features
- Explore restaurants aroud current location
- Pins show the name of the place
- Display details of the place with phone, photo, website, rating
- Call the place or visit website if phone is not available

### Installation

- Clone or download the repository and build the `ForsquareDott` scheme. The application doesn't require any dependencies. 
- Insert your client secret and id in the `APIKeys`

### Fastlane

The project uses Fastlane to simplify testing and code style validation (can be used on CI). To install Fastlane, please follow official instructions [here](https://docs.fastlane.tools) 

To run the `commit_stage` lane first, you need to instal Fastlane dependencies using bundler:

```sh
$ bundle install
```

Then you can run the lane:

```sh
$ bundle exec fastlane commit_stage scheme:"ForsquareDott"
```

### Todos

 - Performance improvements and smart clustering
 - More comprehensive details displaying
 - Cache images
 - Write more tests
 - Watch support 😅
 - ???

**Enjoy!**
